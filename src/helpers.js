
export const feelings2Row = (feel, id) => ({
  id: feel.id,
  desc: feel.desc,
  imgUrl: feel.imgUrl
});
