import React, { Component } from 'react';
import SimpleDialog from './Dialog'
import { Paper, CircularProgress, TableRow, Table, TableHead, TableCell, TableBody, Button, withStyles } from '@material-ui/core';
import { feelings2Row } from "./helpers";
import './App.css';

const map = (list, func) => (list || []).map(func);

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

class App extends Component {

  state = {
    open: false,
    selectedValue: {},
    loading: true,
    feelings: [],
    url: 'http://54.196.43.91:3000/raccoon/feelings'
  };

  componentDidMount(){
    this.getFeelings();
  }

  getFeelings = () => {
    this.setState({ loading: true });
    fetch(this.state.url)
      .then(res=>res.json())
      .then(res => {
        this.setState({
          feelings: res.feelings,
          loading: false
        })
      })
  };

  handleClickOpen = (id, desc, imgUrl) => {
    this.setState({
      open: true,
      selectedValue: {id, desc, imgUrl}
    });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleOption = (id, desc) => {
    this.setState({
      open: false
    });
    alert(`Bien! Seleccionaste ${desc}`)
  };


  render() {
    const feels = map(this.state.feelings, feelings2Row);
    if (this.state.loading){
    return (
      <div className="App">

        <body className="App-body">
          <Paper elevation={2} style={{width: '90%', minHeight: '90%'}}>
          <p style={{color: '#433c3e'}}>
            Obteniendo lista de sentimientos.
          </p>
          <CircularProgress/>
          </Paper>
        </body>

      </div>
    );}
    return(
      <div className="App">
        <body className="App-body">
        <Paper elevation={2}>
          <Table>
            <TableHead>
              <TableRow className={'TableRow'}>
                <TableCell>#</TableCell>
                <TableCell>Me siento...</TableCell>
                <TableCell align='left'>Acciones</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {feels.map(row => (
                <TableRow key={row.id}>
                  <TableCell component="th" scope="row">
                    {row.id}
                  </TableCell>
                  <TableCell align="left">{row.desc}</TableCell>
                  <TableCell align="center">
                    <Button style={{marginRight: '1vh'}} variant="contained" color="secondary" onClick={() => this.handleClickOpen(row.id, row.desc, row.imgUrl)}>Detalles</Button>
                    <Button variant="contained" color="secondary" onClick={() => this.handleOption(row.id, row.desc, row.imgUrl)}>Seleccionar</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
        <SimpleDialog
          selectedValue={this.state.selectedValue}
          details={this.state.selectedValue}
          open={this.state.open}
          onClose={this.handleClose}
          onSelect={this.handleOption}
        />
        </body>
      </div>
    )
  }
}

export default withStyles(styles)(App);
