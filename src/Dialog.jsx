import React, { Component } from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import withStyles from "@material-ui/core/es/styles/withStyles"
import blue from "@material-ui/core/es/colors/blue";
import DialogActions from '@material-ui/core/DialogActions';
import Button from "@material-ui/core/Button/Button";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
const styles = {
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
};

class SimpleDialog extends Component {
  handleClose = () => {
    this.props.onClose(this.props.selectedValue);
  };

  render() {
    const { onSelect, ...other } = this.props;
    const { imgUrl, desc, id } = this.props.details;
    return (
      <Dialog onClose={this.handleClose} aria-labelledby="simple-dialog-title" {...other}>
        <DialogContent style={{width: '40vh', height: '30vh'}}>
          <img alt={desc} src={imgUrl} style={{ width: '60%', display: 'block', marginLeft: 'auto', marginRight: 'auto'}}/>
          <DialogContentText id="alert-dialog-description">
            Te sientes {desc}?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} variant={'outlined'} color={'secondary'}>Cerrar</Button>
          <Button onClick={() => onSelect(id, desc)} variant={'contained'} color={'secondary'}>Justo asi!</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles)(SimpleDialog);